package edu.byu.hbll.jeedemo.dao;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.UpdateOptions;

@Stateless
public class StoreDAO {
    
    private MongoCollection<Document> collection;
    
    @PostConstruct
    public void init() {
        // MongoClient client = new MongoClient(new
        // MongoClientURI("mongodb://jeedemouser:jeedemouser$1@mongodb/jeedemo"));
        MongoClient client = new MongoClient();
        MongoDatabase db = client.getDatabase("jeedemo");
        collection = db.getCollection("store");
    }
    
    public void putMessage(String key, String value) {
        Document filter = new Document().append("_id", key);
        Document update = new Document("$set", new Document().append("value", value));
        collection.updateOne(filter, update, new UpdateOptions().upsert(true));
    }
    
    public String getMessage(String key) {
        Document filter = new Document().append("_id", key);
        Document doc = collection.find(filter).first();
        if (doc == null) {
            return "";
        }
        return doc.getString("value");
    }
    
    public List<String> getAll() {
        MongoIterable<Document> it = collection.find();
        List<String> results = new LinkedList<>();
        for (Document d : it) {
            results.add(d.getString("value"));
        }
        return results;
    }
}
