package edu.byu.hbll.jeedemo.api;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.byu.hbll.jeedemo.dao.StoreDAO;

@Path("store")
public class StoreAPI {
    
    @Inject
    private StoreDAO store;
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getItem(@QueryParam("key") String key) {
        String value = store.getMessage(key);
        return Response.ok(value).build();
    }
    
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Response putItem(@QueryParam("key") String key,
            @QueryParam("value") String value)
    {
        store.putMessage(key, value);
        return Response.ok().build();
    }
    
    @GET
    @Path("all")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getAll() {
        String resp = listToString(store.getAll());
        return Response.ok(resp).build();
    }
    
    private String listToString(List<String> list) {
        return String.join("\n", list);
    }
}
