package edu.byu.hbll.jeedemo.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("demo")
public class DemoAPI {
    
    @GET
    public Response demo() throws Exception {
        throw new Exception();
    }
}
